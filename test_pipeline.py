import os
import imp
import new
import pytest
BASE_PATH = os.path.dirname(os.path.realpath(__file__))

class sysconfig:
	def __init__(self):
		self.gun = 'gun1'
		self.mop = 's1'

	def get_gun(self):
		return self.gun

	def get_mop(self):
		return self.mop

tdb = 'sample.db'

class TestAll:
	def run(self, test_module):
		test = test_module.split('.')[0]
		sc = sysconfig()
		newsrc = os.path.join(BASE_PATH, test_module)  # path to test script
		py_mod = imp.load_source(test, newsrc)
		class_inst = getattr(py_mod, test)
		obj = new.instance(class_inst)
		init_ = getattr(obj, '__init__')
		init_(sc)
		testrunner = getattr(obj, 'run')
		return testrunner(tdb)

	def test_1(self):
		assert self.run('sub1.py')

	def test_2(self):
		assert self.run('sub2.py')

	def test_3(self):
		assert self.run('sub3.py')