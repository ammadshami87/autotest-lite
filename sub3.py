
import os
import socket

class sub3:
	def __init__(self, sysconfig):
		self.sc = sysconfig
		print 'The gun is: %s' % self.sc.get_gun()

	def run(self, tdb):
		connected = True
		host = socket.gethostname()  # as both code is running on same pc
		port = 5000  # socket server port number
		try:
			client_socket = socket.socket()  # instantiate
			client_socket.connect((host, port))  # connect to the local server
		except:
			#print("Connection failed!")
			connected = False
		# update log file
		filename = '/home/dragon/autotest-lite/test.log'
		if os.path.exists(filename):
			flag = 'a'
		else:
			flag = 'w'
		logfile = open(filename, 'a')
		text = 'Pass' if connected else 'Fail'
		logfile.write(text + '\n')
		logfile.close()
		return connected